import { logger } from './logger';

export enum StandardErrorType {
	unknown = 'Unknown',
	missingEnvVar = 'Missing env var',
	invalidEnvVar = 'Invalid env var',
	profileNotFound = 'Profile not found',
	missingArg = 'Missing argument',
	missingCall = 'Missing call',
	invalidAuth = 'Invalid authentication'
}

export class StandardError extends Error {
	public type: StandardErrorType;
	public details?: Record<string, unknown>;

	public constructor(
		type: StandardErrorType = StandardErrorType.unknown,
		message: string,
		details?: Record<string, unknown>
	) {
		super(message);
		// eslint-disable-next-line max-len
		// https://github.com/Microsoft/TypeScript/wiki/Breaking-Changes#extending-built-ins-like-error-array-and-map-may-no-longer-work
		Object.setPrototypeOf(this, StandardError.prototype);
		if (type === StandardErrorType.unknown) {
			logger.warn('Using custom error, please create a new type if needed', { at: this.stack });
		}
		this.type = type;
		this.details = details;
	}

	public static missingEnvVar(envVar: string): StandardError {
		return new StandardError(StandardErrorType.missingEnvVar, `Missing env variable ${envVar}`);
	}

	public static invalidEnvVar(envVar: string, reason: string): StandardError {
		return new StandardError(StandardErrorType.invalidEnvVar, `Invalid env variable ${envVar}, ${reason}`);
	}

	public static profileNotFound(id: string, details?: string): StandardError {
		return new StandardError(StandardErrorType.profileNotFound, `Id ${id} not found. ${details}`);
	}

	public static missingArg(arg: string): StandardError {
		return new StandardError(StandardErrorType.missingArg, `Missing argument ${arg}`);
	}

	public static missingCall(forgotten: string, where?: string): StandardError {
		if (where) {
			return new StandardError(
				StandardErrorType.missingCall, `Missing call to ${forgotten} before calling ${where}`
			);
		}
		return new StandardError(StandardErrorType.missingCall, `Missing call to ${forgotten}`);
	}

	public static invalidAuth(details?: Record<string, unknown>): StandardError {
		return new StandardError(StandardErrorType.invalidAuth, 'Invalid authentication', details);
	}
}
