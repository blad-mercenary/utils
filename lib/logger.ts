import pino from 'pino';

const mode = (typeof process !== 'undefined') ? 'node' : 'front';

function getLogger(levels: string[]) {
	if (mode === 'node') {
		const manualLevel = process.env.BLAD_LOGLEVEL;
		const level = (manualLevel && levels.includes(manualLevel))
			? manualLevel
			: 'info';

		const logfile = process.env.BLAD_LOGFILE || (process.env ? './blad.log' : undefined);

		return {
			logger: pino(
				{
					name: 'BLaD',
					level
				},
				pino.destination(logfile)
			),
			level,
			node: true,
			default: process.env.BLAD_LOGLEVEL
		};
	} else {
		const level = 'info';
		return {
			logger: pino(
				{
					name: 'BLaD',
					level
				}
			),
			level,
			node: false,
			default: true
		};
	}
}

const defaultPinoLevels = pino.levels.values;
const customLevels: {[index: string]: number} = {};

const dPL = Object.keys(defaultPinoLevels).map((value): { label: string; value: number } => {
	return { label: value, value: defaultPinoLevels[value] };
});
const cL = Object.keys(customLevels).map((value): { label: string; value: number } => {
	return { label: value, value: customLevels[value] };
});

export const levels = dPL.concat(cL).sort((a, b): number => {
	return a.value - b.value;
}).map((value): string => value.label).concat('silent');

const { logger: _logger, level } = getLogger(levels);

// eslint-disable-next-line @typescript-eslint/no-explicit-any
function log(level: pino.Level, message: string, details: any = {}, ...args: any[]): void {
	_logger[level](details, message, ...args);
}

export const logger = {
	trace: log.bind(undefined, 'trace'),
	debug: log.bind(undefined, 'debug'),
	info: log.bind(undefined, 'info'),
	warn: log.bind(undefined, 'warn'),
	error: log.bind(undefined, 'error'),
	fatal: log.bind(undefined, 'fatal')
};

if (mode === 'node') {
	logger.debug(`Logger instanciated with level ${level}, ${
		(process?.env?.LOGLEVEL)?
			`set to ${process?.env?.LOGLEVEL}`
			: 'using default'
	}`);
} else {
	logger.debug('Logger started');
}

