"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
exports.StandardError = exports.StandardErrorType = void 0;
var logger_1 = require("./logger");
var StandardErrorType;
(function (StandardErrorType) {
    StandardErrorType["unknown"] = "Unknown";
    StandardErrorType["missingEnvVar"] = "Missing env var";
    StandardErrorType["invalidEnvVar"] = "Invalid env var";
    StandardErrorType["profileNotFound"] = "Profile not found";
    StandardErrorType["missingArg"] = "Missing argument";
    StandardErrorType["missingCall"] = "Missing call";
    StandardErrorType["invalidAuth"] = "Invalid authentication";
})(StandardErrorType = exports.StandardErrorType || (exports.StandardErrorType = {}));
var StandardError = (function (_super) {
    __extends(StandardError, _super);
    function StandardError(type, message, details) {
        if (type === void 0) { type = StandardErrorType.unknown; }
        var _this = _super.call(this, message) || this;
        Object.setPrototypeOf(_this, StandardError.prototype);
        if (type === StandardErrorType.unknown) {
            logger_1.logger.warn('Using custom error, please create a new type if needed', { at: _this.stack });
        }
        _this.type = type;
        _this.details = details;
        return _this;
    }
    StandardError.missingEnvVar = function (envVar) {
        return new StandardError(StandardErrorType.missingEnvVar, "Missing env variable ".concat(envVar));
    };
    StandardError.invalidEnvVar = function (envVar, reason) {
        return new StandardError(StandardErrorType.invalidEnvVar, "Invalid env variable ".concat(envVar, ", ").concat(reason));
    };
    StandardError.profileNotFound = function (id, details) {
        return new StandardError(StandardErrorType.profileNotFound, "Id ".concat(id, " not found. ").concat(details));
    };
    StandardError.missingArg = function (arg) {
        return new StandardError(StandardErrorType.missingArg, "Missing argument ".concat(arg));
    };
    StandardError.missingCall = function (forgotten, where) {
        if (where) {
            return new StandardError(StandardErrorType.missingCall, "Missing call to ".concat(forgotten, " before calling ").concat(where));
        }
        return new StandardError(StandardErrorType.missingCall, "Missing call to ".concat(forgotten));
    };
    StandardError.invalidAuth = function (details) {
        return new StandardError(StandardErrorType.invalidAuth, 'Invalid authentication', details);
    };
    return StandardError;
}(Error));
exports.StandardError = StandardError;
