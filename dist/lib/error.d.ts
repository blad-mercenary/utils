export declare enum StandardErrorType {
    unknown = "Unknown",
    missingEnvVar = "Missing env var",
    invalidEnvVar = "Invalid env var",
    profileNotFound = "Profile not found",
    missingArg = "Missing argument",
    missingCall = "Missing call",
    invalidAuth = "Invalid authentication"
}
export declare class StandardError extends Error {
    type: StandardErrorType;
    details?: Record<string, unknown>;
    constructor(type: StandardErrorType | undefined, message: string, details?: Record<string, unknown>);
    static missingEnvVar(envVar: string): StandardError;
    static invalidEnvVar(envVar: string, reason: string): StandardError;
    static profileNotFound(id: string, details?: string): StandardError;
    static missingArg(arg: string): StandardError;
    static missingCall(forgotten: string, where?: string): StandardError;
    static invalidAuth(details?: Record<string, unknown>): StandardError;
}
