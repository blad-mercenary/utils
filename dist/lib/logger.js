"use strict";
var __spreadArray = (this && this.__spreadArray) || function (to, from, pack) {
    if (pack || arguments.length === 2) for (var i = 0, l = from.length, ar; i < l; i++) {
        if (ar || !(i in from)) {
            if (!ar) ar = Array.prototype.slice.call(from, 0, i);
            ar[i] = from[i];
        }
    }
    return to.concat(ar || Array.prototype.slice.call(from));
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
var _a, _b;
Object.defineProperty(exports, "__esModule", { value: true });
exports.logger = exports.levels = void 0;
var pino_1 = __importDefault(require("pino"));
var mode = (typeof process !== 'undefined') ? 'node' : 'front';
function getLogger(levels) {
    if (mode === 'node') {
        var manualLevel = process.env.BLAD_LOGLEVEL;
        var level_1 = (manualLevel && levels.includes(manualLevel))
            ? manualLevel
            : 'info';
        var logfile = process.env.BLAD_LOGFILE || (process.env ? './blad.log' : undefined);
        return {
            logger: (0, pino_1.default)({
                name: 'BLaD',
                level: level_1
            }, pino_1.default.destination(logfile)),
            level: level_1,
            node: true,
            default: process.env.BLAD_LOGLEVEL
        };
    }
    else {
        var level_2 = 'info';
        return {
            logger: (0, pino_1.default)({
                name: 'BLaD',
                level: level_2
            }),
            level: level_2,
            node: false,
            default: true
        };
    }
}
var defaultPinoLevels = pino_1.default.levels.values;
var customLevels = {};
var dPL = Object.keys(defaultPinoLevels).map(function (value) {
    return { label: value, value: defaultPinoLevels[value] };
});
var cL = Object.keys(customLevels).map(function (value) {
    return { label: value, value: customLevels[value] };
});
exports.levels = dPL.concat(cL).sort(function (a, b) {
    return a.value - b.value;
}).map(function (value) { return value.label; }).concat('silent');
var _c = getLogger(exports.levels), _logger = _c.logger, level = _c.level;
function log(level, message, details) {
    if (details === void 0) { details = {}; }
    var args = [];
    for (var _i = 3; _i < arguments.length; _i++) {
        args[_i - 3] = arguments[_i];
    }
    _logger[level].apply(_logger, __spreadArray([details, message], args, false));
}
exports.logger = {
    trace: log.bind(undefined, 'trace'),
    debug: log.bind(undefined, 'debug'),
    info: log.bind(undefined, 'info'),
    warn: log.bind(undefined, 'warn'),
    error: log.bind(undefined, 'error'),
    fatal: log.bind(undefined, 'fatal')
};
if (mode === 'node') {
    exports.logger.debug("Logger instanciated with level ".concat(level, ", ").concat(((_a = process === null || process === void 0 ? void 0 : process.env) === null || _a === void 0 ? void 0 : _a.LOGLEVEL) ?
        "set to ".concat((_b = process === null || process === void 0 ? void 0 : process.env) === null || _b === void 0 ? void 0 : _b.LOGLEVEL)
        : 'using default'));
}
else {
    exports.logger.debug('Logger started');
}
