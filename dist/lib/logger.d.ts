export declare const levels: string[];
export declare const logger: {
    trace: (message: string, details?: any, ...args: any[]) => void;
    debug: (message: string, details?: any, ...args: any[]) => void;
    info: (message: string, details?: any, ...args: any[]) => void;
    warn: (message: string, details?: any, ...args: any[]) => void;
    error: (message: string, details?: any, ...args: any[]) => void;
    fatal: (message: string, details?: any, ...args: any[]) => void;
};
