import { describe, it } from 'mocha';
import * as chai from 'chai';
import { logger } from '../../lib/logger';
import * as sinon from 'sinon';
import sinonChai from 'sinon-chai';

chai.use(sinonChai);

const { expect } = chai;

import { StandardError, StandardErrorType } from '../../lib/error';

describe('error', (): void => {
	describe('StandardError', (): void => {
		describe('constructor', (): void => {
			afterEach((): void => {
				sinon.restore();
			});

			it('log a warning on use of type unknown', (): void => {
				sinon.stub(logger, 'warn');
				new StandardError(StandardErrorType.unknown, 'test');
				expect(logger.warn).to.have.been.called;
			});
		});

		describe('return Error on everything', (): void => {
			it('missingEnvVar', (): void => {
				const error = StandardError.missingEnvVar('test');
				expect(error).to.be.an.instanceOf(Error);
				expect(error).to.be.an.instanceOf(StandardError);
				expect(error.type).to.equal(StandardErrorType.missingEnvVar);
			});

			it('invalidEnvVar', (): void => {
				const error = StandardError.invalidEnvVar('test', 'test');
				expect(error).to.be.an.instanceOf(Error);
				expect(error).to.be.an.instanceOf(StandardError);
				expect(error.type).to.equal(StandardErrorType.invalidEnvVar);
			});

			it('profileNotFound', (): void => {
				const error = StandardError.profileNotFound('test');
				expect(error).to.be.an.instanceOf(Error);
				expect(error).to.be.an.instanceOf(StandardError);
				expect(error.type).to.equal(StandardErrorType.profileNotFound);
			});

			it('missingArg', (): void => {
				const error = StandardError.missingArg('test');
				expect(error).to.be.an.instanceOf(Error);
				expect(error).to.be.an.instanceOf(StandardError);
				expect(error.type).to.equal(StandardErrorType.missingArg);
			});

			it('missingCall', (): void => {
				const error = StandardError.missingCall('test');
				expect(error).to.be.an.instanceOf(Error);
				expect(error).to.be.an.instanceOf(StandardError);
				expect(error.type).to.equal(StandardErrorType.missingCall);
			});

			it('invalidAuth', (): void => {
				const error = StandardError.invalidAuth({});
				expect(error).to.be.an.instanceOf(Error);
				expect(error).to.be.an.instanceOf(StandardError);
				expect(error.type).to.equal(StandardErrorType.invalidAuth);
			});
		});
	});
});
